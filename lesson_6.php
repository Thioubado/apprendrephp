<?php
$toto = 512;

// on enchaine les controles ci-dessous
if($toto > 0 && $toto < 499)  // 1er
{
    echo "$toto est compris entre 0 et 499";
}elseif ($toto >= 500 && $toto < 1000) // 2eme
{
    echo "$toto est compris entre 500 et 999";
}else{ // 3eme
    echo "$toto est plus que 999";
}

echo "<hr/>";

$medor = "chien";

if($medor == 'girafe')
{
    echo "Medor est une girafe";
}elseif($medor == 'elephant')
{
    echo "Medor est un elephant";
}elseif($medor == 'souris')
{
    echo "Medor est une souris";
}elseif($medor == 'chien')
{
    echo "Medor est un chien";
}
elseif($medor == 'chat')
{
    echo "Medor est un chat";
}else{
    echo "Peut etre un hippopotame ? Qui sait ...";
}

echo "<hr/>";

$medar = 'chien';
switch($medar)
{
    case 'girafe':
        echo "Medar est une girafe !";
    break;
    case 'elephant':
        echo "Medar est un elephant";
    break;
    case 'souris':
        echo "Medar est une souris";
    break;
    case 'chien':
        echo "Medar est un chien";
    break;
    case 'chat':
        echo "Medar est un chat";
    break;
    default:
    echo "Peut etre un hippopotame ? Qui sait ...";
}

echo "<hr/>";

$popo = 6;
$i = 0;

while($i != $popo)
{
    echo "Popo est different de $i<br>";
    $i++;
}
echo "Popo est egal $i";

echo "<hr>";

$pupu = 6;

for($i=0; $i != $pupu; $i++)
{
    echo "Pupu est different de $i<br>";
}
echo "Pupu est egal $i";